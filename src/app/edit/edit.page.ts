import { Component, OnInit } from '@angular/core';
import * as firebase from 'Firebase';
import { ActivatedRoute, Router  } from '@angular/router';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {

  ref = firebase.database().ref('todos/');
  todoForm: FormGroup;

  constructor(private route: ActivatedRoute,
    public router: Router,
    private formBuilder: FormBuilder) {
      this.todoForm = this.formBuilder.group({
        'todo_title' : [null, Validators.required],
        'todo_description' : [null, Validators.required]
      });
      this.getTodo(this.route.snapshot.paramMap.get('key'));
    }

  ngOnInit() {
  }

  getTodo(key) {
    firebase.database().ref('todos/'+key).on('value', resp => {
      let todo = snapshotToObject(resp);
      this.todoForm.controls['todo_title'].setValue(todo.todo_title);
      this.todoForm.controls['todo_description'].setValue(todo.todo_description);
    });
  }

  updateTodo() {
    let newTodo = firebase.database().ref('todos/'+this.route.snapshot.paramMap.get('key')).update(this.todoForm.value);
    this.router.navigate(['/detail/'+this.route.snapshot.paramMap.get('key')]);
  }

}

export const snapshotToObject = snapshot => {
  let item = snapshot.val();
  item.key = snapshot.key;

  return item;
}
