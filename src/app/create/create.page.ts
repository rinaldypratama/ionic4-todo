import { Component, OnInit } from '@angular/core';
import * as firebase from 'Firebase';
import { ActivatedRoute, Router  } from '@angular/router';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-create',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})
export class CreatePage implements OnInit {

  ref = firebase.database().ref('todos/');
  todoForm: FormGroup;

  constructor(private route: ActivatedRoute,
    public router: Router,
    private formBuilder: FormBuilder) {
      this.todoForm = this.formBuilder.group({
        'todo_title' : [null, Validators.required],
        'todo_description' : [null, Validators.required]
      });
    }

  ngOnInit() {
  }

  saveTodo() {
    let newTodo = firebase.database().ref('todos/').push();
    newTodo.set(this.todoForm.value);
    this.router.navigate(['/detail/'+newTodo.key]);
  }

}
